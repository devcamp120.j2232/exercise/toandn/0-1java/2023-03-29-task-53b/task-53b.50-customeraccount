import models.Account;
import models.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Teo", 20);
        Customer customer2 = new Customer(2, "Bich", 30);
        System.out.println("Customer 1: " + customer1.toString());
        System.out.println("Customer 2: " + customer2.toString());
        Account account1 = new Account(1, customer1, 100000);
        Account account2 = new Account(2, customer2, 120000);
        System.out.println("Account 1: " + account1.toString());
        System.out.println("Account 2: " + account2.toString());
    }
}
